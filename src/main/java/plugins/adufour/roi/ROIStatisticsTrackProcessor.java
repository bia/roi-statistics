package plugins.adufour.roi;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.poi.ss.usermodel.Workbook;
import org.math.plot.Plot2DPanel;

import icy.file.FileUtil;
import icy.gui.dialog.MessageDialog;
import icy.gui.dialog.SaveDialog;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.util.GuiUtil;
import icy.plugin.interface_.PluginBundled;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import plugins.adufour.blocks.tools.io.WorkbookToFile;
import plugins.adufour.blocks.tools.io.WorkbookToFile.MergePolicy;
import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarChannel;
import plugins.adufour.vars.lang.VarROIDescriptor;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarListener;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;

public class ROIStatisticsTrackProcessor extends PluginTrackManagerProcessor implements PluginBundled
{
    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }

    private final JButton exportButton = new JButton("Export to XLS...");

    private VarSequence sequence = new VarSequence("Sequence", null);
    final VarROIDescriptor roiDescriptor = new VarROIDescriptor("Plot:");
    private VarChannel channel = new VarChannel("Channel", sequence, false);

    VarEditor<Integer> channelSelector = channel.createVarEditor();
    private VarEditor<String> roiDescriptorSelector = roiDescriptor.createVarEditor(true);

    private final JPanel chartPanel = new JPanel();
    private final Plot2DPanel plotPanel = new Plot2DPanel();

    public ROIStatisticsTrackProcessor()
    {
        super();

        super.getDescriptor().setDescription("Monitor shape and intensity over time");
        super.setName("ROI Statistics");

        super.panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        super.panel.add(Box.createVerticalStrut(5));
        super.panel.add(GuiUtil.createLineBoxPanel(Box.createHorizontalStrut(10), new JLabel("Plot:"),
                Box.createHorizontalStrut(5), (JComponent) roiDescriptorSelector.getEditorComponent(),
                Box.createHorizontalStrut(10), new JLabel("Channel:"), Box.createHorizontalStrut(5),
                (JComponent) channelSelector.getEditorComponent(), Box.createHorizontalStrut(10), exportButton,
                Box.createHorizontalStrut(10)));
        super.panel.add(chartPanel);

        exportButton.setEnabled(false);

        sequence.addListener(new VarListener<Sequence>()
        {
            @Override
            public void valueChanged(Var<Sequence> source, Sequence oldValue, Sequence newValue)
            {
                channelSelector.setEnabled(ROIDescriptor.getDescriptor(roiDescriptor.getValue()).separateChannel());
                Compute();
            }

            @Override
            public void referenceChanged(Var<Sequence> source, Var<? extends Sequence> oldReference,
                    Var<? extends Sequence> newReference)
            {
                //
            }
        });

        roiDescriptor.addListener(new VarListener<String>()
        {
            @Override
            public void valueChanged(Var<String> source, String oldValue, String newValue)
            {
                channelSelector.setEnabled(ROIDescriptor.getDescriptor(roiDescriptor.getValue()).separateChannel());
                Compute();
            }

            @Override
            public void referenceChanged(Var<String> source, Var<? extends String> oldReference,
                    Var<? extends String> newReference)
            {
                //
            }
        });

        channel.addListener(new VarListener<Integer>()
        {
            @Override
            public void valueChanged(Var<Integer> source, Integer oldValue, Integer newValue)
            {
                Compute();
            }

            @Override
            public void referenceChanged(Var<Integer> source, Var<? extends Integer> oldReference,
                    Var<? extends Integer> newReference)
            {
                //
            }
        });
        exportButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                export();
            }
        });

        plotPanel.setPreferredSize(new Dimension(500, 300));
    }

    @Override
    public void Close()
    {
        channelSelector.setEnabled(false);
    }

    @Override
    public void Compute()
    {
        chartPanel.removeAll();

        if (!super.isEnabled())
            return;

        final Sequence seq = trackPool.getDisplaySequence();

        try
        {
            ROITrackStatistics.buildPlot(trackPool.getTrackGroupList(), seq, channel.getValue().intValue(),
                    ROIDescriptor.getDescriptor(roiDescriptor.getValue()), plotPanel);

            if (plotPanel.getPlots().size() > 0)
            {
                chartPanel.add(plotPanel);
                exportButton.setEnabled(true);
            }
            else
            {
                chartPanel.add(
                        new JLabel("No statistics available. This may happen when tracking particles (with no shape)"));
                exportButton.setEnabled(false);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            chartPanel.add(new JLabel("Cannot compute descriptor: " + e.getMessage()));
            exportButton.setEnabled(false);
        }

        super.panel.updateUI();
    }

    @Override
    public void displaySequenceChanged()
    {
        sequence.setValue(trackPool.getDisplaySequence());
        Compute();
    }

    private void export()
    {
        if (plotPanel.getPlots().size() == 0)
            return;

        new Thread()
        {
            @Override
            public void run()
            {
                final Sequence seq = trackPool.getDisplaySequence();
                final String name = ROIDescriptor.getDescriptor(roiDescriptor.getValue()).getName();

                // Restore last used folder
                final String xlsFolder = getPreferencesRoot().get("xlsFolder", null);
                final String path = SaveDialog.chooseFile("Export statistics", xlsFolder, name, ".xls");

                // canceled
                if (path == null)
                    return;

                // store the folder in the preferences
                getPreferencesRoot().put("xlsFolder", FileUtil.getDirectory(path));

                final AnnounceFrame message = new AnnounceFrame("Exporting statistics...", 0);
                try
                {
                    final Workbook wb = ROITrackStatistics.getWorkBook(seq, name, plotPanel);

                    if (wb != null)
                    {
                        WorkbookToFile.saveAsSpreadSheet(wb, path, MergePolicy.Overwrite);
                        wb.close();
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    message.close();
                }
            }
        }.start();
    }
}
