package plugins.adufour.roi;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.type.point.Point3D;
import plugins.adufour.quickhull.QuickHull2D;
import plugins.adufour.quickhull.QuickHull3D;
import plugins.adufour.roi.mesh.Vertex3D;
import plugins.adufour.roi.mesh.polygon.ROI3DPolygonalMesh;
import plugins.kernel.roi.roi2d.ROI2DRectShape;

public class ROIConvexHullDescriptor extends Plugin implements PluginROIDescriptor, PluginBundled
{
    public static class ROIConvexity extends ROIDescriptor
    {
        protected ROIConvexity()
        {
            super("Convexity", Double.class);
        }

        @Override
        public String getDescription()
        {
            String line1 = "<html>Ratio of the ROI area over its convex envelope.";
            String line2 = "<br/>This value is expressed as a percentage and is 100 for a purely convex object.</html>";
            return line1 + line2;
        }

        @Override
        public String getUnit(Sequence sequence)
        {
            return "%";
        }
        
        @Override
        public Object[] getBounds()
        {
            return new Object[] {Double.valueOf(0d), Double.valueOf(100d)};
        }

        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
        {
            return computeConvexity(roi);
        }

        /**
         * @param roi
         *        the roi to compute convexity
         * @return The ratio between the area of the ROI and that of its convex hull (envelope),
         *         expressed as a percentage
         * @throws InterruptedException
         */
        public static double computeConvexity(ROI roi) throws InterruptedException
        {
            if (roi instanceof ROI2DRectShape)
                return 100.0;

            double interior = roi.getNumberOfPoints();

            if (roi.getNumberOfPoints() < 4)
                return 100;

            if (roi instanceof ROI2D)
            {
                ROI2D r2 = (ROI2D) roi;

                Point[] pts = r2.getBooleanMask(true).getContourPoints();

                if (pts.length < 4)
                    return 100;

                interior = 0;

                List<Point2D> points = new ArrayList<Point2D>(pts.length);
                for (Point p : pts)
                    points.add(new Point2D.Double(p.x, p.y));

                points = QuickHull2D.computeConvexEnvelope(points);

                // formulas:
                // contour = sum( sqrt[ (x[i] - x[i-1])^2 + (y[i] - y[i-1])^2 ] )
                // area = 0.5 * sum( (x[i-1] * y[i]) - (y[i-1] * x[i]) )

                Point2D p1 = points.get(points.size() - 1), p2 = null;

                for (int i = 0; i < points.size(); i++)
                {
                    p2 = points.get(i);
                    interior += (p1.getX() * p2.getY()) - (p1.getY() * p2.getX());
                    p1 = p2;
                }

                interior = Math.abs(interior * 0.5);
            }
            else if (roi instanceof ROI3D)
            {
                Point3d[] points = null;

                if (roi instanceof ROI3DPolygonalMesh)
                {
                    ROI3DPolygonalMesh mesh = (ROI3DPolygonalMesh) roi;
                    // Tuple3d res = mesh.getPixelSize();
                    points = new Point3d[mesh.getNumberOfVertices(true)];

                    int v = 0;
                    for (Vertex3D vertex : mesh.getVertices())
                    {
                        if (vertex == null)
                            continue;
                        // points[v++] = new Point3d(vertex.position.x / res.x, vertex.position.y / res.y, vertex.position.z / res.z);
                        points[v++] = new Point3d(vertex.position.x, vertex.position.y, vertex.position.z);
                    }
                }
                else
                {
                    Point3D.Integer[] pts = ((ROI3D) roi).getBooleanMask(true).getContourPoints();

                    if (pts.length < 4)
                        return 100;

                    points = new Point3d[pts.length];
                    for (int i = 0; i < pts.length; i++)
                    {
                        Point3D.Integer p = pts[i];
                        points[i] = new Point3d(p.x, p.y, p.z);
                    }
                }

                try
                {
                    QuickHull3D qhull = new QuickHull3D(points);
                    qhull.triangulate();
                    int[][] hullFaces = qhull.getFaces();
                    Point3d[] hullPoints = qhull.getVertices();

                    Vector3d v12 = new Vector3d();
                    Vector3d v13 = new Vector3d();
                    Vector3d cross = new Vector3d();

                    Vector3d p1 = new Vector3d();

                    interior = 0;

                    for (int[] face : hullFaces)
                    {
                        p1.set(hullPoints[face[0]]);
                        Point3d p2 = hullPoints[face[1]];
                        Point3d p3 = hullPoints[face[2]];

                        v12.sub(p2, p1);
                        v13.sub(p3, p1);
                        cross.cross(v12, v13);

                        double surf = cross.length() * 0.5;

                        cross.normalize();
                        interior += surf * cross.dot(p1) / 3.0;
                    }
                }
                catch (RuntimeException e)
                {
                    System.err.println(
                            "Warning while computing the convexity of " + roi.getName() + ": " + e.getMessage());
                }
            }
            else
            {
                System.err.println("WARNING: cannot compute the convexity of a " + roi.getClassName());
                return Double.NaN;
            }

            return Math.round(100.0 * Math.min(1.0, roi.getNumberOfPoints() / interior));
        }
    }

    private static final ROIConvexity convexity = new ROIConvexity();

    @Override
    public List<ROIDescriptor> getDescriptors()
    {
        ArrayList<ROIDescriptor> list = new ArrayList<ROIDescriptor>();
        list.add(convexity);
        return list;
    }

    @Override
    public Map<ROIDescriptor, Object> compute(ROI roi, Sequence sequence)
            throws UnsupportedOperationException, InterruptedException
    {
        HashMap<ROIDescriptor, Object> map = new HashMap<ROIDescriptor, Object>(1);
        map.put(convexity, convexity.compute(roi, sequence));
        return map;
    }

    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
}
