package plugins.adufour.roi;

import icy.plugin.interface_.PluginBundled;

@Deprecated
public class ROIStatistics extends ROIMeasures implements PluginBundled
{
    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
}