package plugins.adufour.roi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.math.ArrayMath;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.type.DataType;

public class ROIHaralickTextureDescriptor extends Plugin implements PluginROIDescriptor, PluginBundled
{
    private static abstract class ROIHaralickDescriptor extends ROIDescriptor
    {
        protected final int step;

        protected ROIHaralickDescriptor(String name, Class<?> type)
        {
            // By default use a step of 1
            this(name, type, 1);
        }

        protected ROIHaralickDescriptor(String name, Class<?> type, int step)
        {
            super(name, type);
            this.step = step;
        }
    }

    public static class ROIAngularSecondMoment extends ROIHaralickDescriptor
    {
        public ROIAngularSecondMoment()
        {
            super("Texture angular second moment", Double.class);
        }

        public ROIAngularSecondMoment(int step)
        {
            super("Texture angular second moment", Double.class, step);
        }

        @Override
        public String getDescription()
        {
            return "Angular Second Moment (Haralick texture descriptor)";
        }

        @Override
        public boolean separateChannel()
        {
            return true;
        }

        @Override
        public Object[] getBounds()
        {
            return new Object[] {Double.valueOf(0d), Double.valueOf(1d)};
        }

        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
        {
            if (!(roi instanceof ROI2D))
                throw new UnsupportedOperationException("(More than 2)-D ROI are not supported");

            // By default, compute texture features with step 1
            return computeHaralickFeatures(sequence, (ROI2D) roi, step).get(angularSecondMoment);
        }
    }

    public static class ROIContrast extends ROIHaralickDescriptor
    {
        public ROIContrast()
        {
            super("Texture contrast", Double.class);
        }

        public ROIContrast(int step)
        {
            super("Texture contrast", Double.class, step);
        }

        @Override
        public String getDescription()
        {
            return "Contrast (Haralick texture descriptor)";
        }

        @Override
        public boolean separateChannel()
        {
            return true;
        }

        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
        {
            if (!(roi instanceof ROI2D))
                throw new UnsupportedOperationException("(More than 2)-D ROI are not supported");

            return computeHaralickFeatures(sequence, (ROI2D) roi, step).get(contrast);
        }
    }

    public static class ROIEntropy extends ROIHaralickDescriptor
    {
        public ROIEntropy()
        {
            super("Texture entropy", Double.class);
        }

        public ROIEntropy(int step)
        {
            super("Texture entropy", Double.class, step);
        }

        @Override
        public String getDescription()
        {
            return "Entropy (Haralick texture descriptor)";
        }

        @Override
        public boolean separateChannel()
        {
            return true;
        }

        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
        {
            if (!(roi instanceof ROI2D))
                throw new UnsupportedOperationException("(More than 2)-D ROI are not supported");

            return computeHaralickFeatures(sequence, (ROI2D) roi, step).get(entropy);
        }
    }

    public static class ROIHomogeneity extends ROIHaralickDescriptor
    {
        public ROIHomogeneity()
        {
            super("Texture homogeneity", Double.class);
        }

        public ROIHomogeneity(int step)
        {
            super("Texture homogeneity", Double.class, step);
        }

        @Override
        public String getDescription()
        {
            return "Homogeneity (Haralick texture descriptor)";
        }

        @Override
        public boolean separateChannel()
        {
            return true;
        }

        @Override
        public Object[] getBounds()
        {
            return new Object[] {Double.valueOf(0d), Double.valueOf(1d)};
        }

        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
        {
            if (!(roi instanceof ROI2D))
                throw new UnsupportedOperationException("(More than 2)-D ROI are not supported");

            return computeHaralickFeatures(sequence, (ROI2D) roi, step).get(homogeneity);
        }
    }

    public static final ROIDescriptor angularSecondMoment = new ROIAngularSecondMoment();
    public static final ROIDescriptor contrast = new ROIContrast();
    public static final ROIDescriptor entropy = new ROIEntropy();
    public static final ROIDescriptor homogeneity = new ROIHomogeneity();

    @Override
    public List<ROIDescriptor> getDescriptors()
    {
        ArrayList<ROIDescriptor> descriptors = new ArrayList<ROIDescriptor>();

        descriptors.add(angularSecondMoment);
        descriptors.add(contrast);
        descriptors.add(entropy);
        descriptors.add(homogeneity);

        return descriptors;
    }

    @Override
    public Map<ROIDescriptor, Object> compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
    {
        if (!(roi instanceof ROI2D))
            throw new UnsupportedOperationException("(More than 2)-D ROI are not supported");

        // By default, compute texture features with step 1
        return computeHaralickFeatures(sequence, (ROI2D) roi, 1);
    }

    /**
     * Calculates the Haralick texture descriptors for the specified ROI
     * 
     * @param step
     *        the step (in pixels) with which the texture analysis should be performed
     * @param sequence
     *        the sequence where the texture should be analyzed
     * @param roi
     *        a 2D ROI where texture should be computed.<br>
     *        <b>NB: if the specified sequence has more than one slice or frame, ROI having Z=-1 or T=-1 are not supported</b>
     * @return A dictionary relating each available {@link ROIDescriptor} to its calculated value
     *         (see {@link #getDescriptors()}) for a full list
     * @throws InterruptedException
     */
    public static Map<ROIDescriptor, Object> computeHaralickFeatures(Sequence sequence, ROI2D roi, int step) throws InterruptedException
    {
        int c = roi.getC();
        if (c == -1)
            throw new UnsupportedOperationException("Texture can only be calculated on a single channel");

        int t = roi.getT();
        if (t == -1)
        {
            if (sequence.getSizeT() > 1)
                throw new UnsupportedOperationException("Texture can only be calculated on a single frame");
            t = 0;
        }

        int z = roi.getZ();
        if (z == -1)
        {
            if (sequence.getSizeZ() > 1)
                throw new UnsupportedOperationException("Texture can only be calculated on a single slice");
            z = 0;
        }

        Map<ROIDescriptor, Object> descriptors = new HashMap<ROIDescriptor, Object>();

        IcyBufferedImage image = sequence.getImage(t, z, c);

        if (image == null)
            return descriptors;

        IcyBufferedImage cooc = buildGLCM(image, roi, step);

        // Retrieve the (linearized) matrix
        double[] _cooc = cooc.getDataXYAsDouble(0);

        // FEATURES

        // Second angular moment
        double _angularSecondMoment = 0;
        // Contrast (inertia)
        double _contrast = 0;
        // Entropy
        double _entropy = 0;
        // Homogeneity (inverse difference moment)
        double _homogeneity = 0;

        int diagOffset = 0, offset = 1;
        for (int j = 0; j < 256; j++)
        {
            double value = _cooc[diagOffset];

            // Accumulate the diagonal values (once)
            _angularSecondMoment += value * value;
            _entropy += value * Math.log(value + 0.0001);
            _homogeneity += value;

            for (int i = j + 1; i < 256; i++, offset++)
            {
                value = _cooc[offset];

                // Accumulate non-diagonal values twice (we only sweep the
                // upper-triangular half)
                _angularSecondMoment += 2 * value * value;
                _entropy += 2 * value * Math.log(value + 0.0001);

                double ij2 = (i - j) * (i - j);
                _homogeneity += 2 * value / (1 + ij2);
                _contrast += 2 * value * ij2;
            }

            // Move to the next line in the upper-triangular half
            offset += j + 2;
            // Move to the next diagonal value
            diagOffset += 257;
        }

        descriptors.put(angularSecondMoment, _angularSecondMoment);
        descriptors.put(contrast, _contrast);
        descriptors.put(entropy, -_entropy);
        descriptors.put(homogeneity, _homogeneity);

        return descriptors;
    }

    /**
     * @param image
     * @return an {@link IcyBufferedImage} representing the gray-level cooccurrence matrix of the
     *         input region of interest (ROI)
     * @throws InterruptedException
     */
    private static IcyBufferedImage buildGLCM(IcyBufferedImage image, ROI2D roi, int step) throws InterruptedException
    {
        // Rescale to [0-255] if necessary
        IcyBufferedImage imByte = null;
        if (image.getDataType_() == DataType.UBYTE)
        {
            imByte = image;
        }
        else
        {
            image.updateChannelsBounds();
            imByte = IcyBufferedImageUtil.convertToType(image, DataType.UBYTE, true, false);
        }

        // That allows us to build a 256^256 co-occurrence matrix
        double[] _cooc = new double[256 * 256];
        // wrap the array into an image (useful for visualization)
        IcyBufferedImage coocImage = new IcyBufferedImage(256, 256, new double[][] {_cooc});

        // build the matrix with the step parameter d
        int imageWidth = imByte.getSizeX();
        int imageHeight = imByte.getSizeY();
        byte[] data = imByte.getDataXYAsByte(0);

        BooleanMask2D mask = roi.getBooleanMask(true);

        int val, val256, neighborVal;
        for (int y = 0, offset = 0; y < imageHeight; y++)
            for (int x = 0; x < imageWidth; x++, offset++)
            {
                if (!mask.contains(x, y))
                    continue;

                val = data[offset] & 0xff;
                val256 = val * 256;

                // horizontal neighbor: 0 degrees
                if (x - step >= 0 && mask.contains(x - step, y))
                {
                    neighborVal = data[offset - step] & 0xff;

                    // increment the co-occurrence matrix (symmetrically)
                    _cooc[val + neighborVal * 256]++;
                    _cooc[neighborVal + val256]++;
                }

                // vertical neighbor: 90 degree
                if (y - step >= 0 && mask.contains(x, y - step))
                {
                    int offsetAbove = offset - imageWidth;

                    neighborVal = data[offsetAbove] & 0xff;
                    // increment the co-occurrence matrix (symmetrically)
                    _cooc[val + neighborVal * 256]++;
                    _cooc[neighborVal + val256]++;

                    // 45 degree (forward) diagonal neighbor
                    if (x + step < imageWidth && mask.contains(x + step, y))
                    {
                        neighborVal = data[offsetAbove + step] & 0xff;
                        // increment the co-occurrence matrix (symmetrically)
                        _cooc[val + neighborVal * 256]++;
                        _cooc[neighborVal + val256]++;
                    }

                    // 135 degree (backward) diagonal neighbor
                    if (x - step >= 0 && mask.contains(x - step, y))
                    {
                        neighborVal = data[offsetAbove - step] & 0xff;
                        // increment the co-occurrence matrix (symmetrically)
                        _cooc[val + neighborVal * 256]++;
                        _cooc[neighborVal + val256]++;
                    }
                }
            }

        // Normalize the matrix by the number of comparisons
        ArrayMath.divide(_cooc, ArrayMath.sum(_cooc), _cooc);

        // Uncomment the following 2 lines to display the GLCM
        // coocImage.dataChanged();
        // addSequence(new Sequence("Grey level co-occurrence matrix", coocImage));

        return coocImage;
    }

    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
}
