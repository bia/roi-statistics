package plugins.adufour.roi;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.math.plot.Plot2DPanel;
import org.math.plot.plots.Plot;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.type.collection.CollectionUtil;
import plugins.adufour.activecontours.ActiveContour;
import plugins.adufour.activecontours.ActiveContours.ROIType;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarROIDescriptor;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.vars.util.VarException;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DPoint;
import plugins.kernel.roi.roi3d.ROI3DArea;
import plugins.kernel.roi.roi3d.ROI3DPoint;
import plugins.nchenouard.particletracking.DetectionSpotTrack;
import plugins.nchenouard.particletracking.legacytracker.SpotTrack;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.Point3D;

/**
 * ROI Statistics export for Tracking (require {@link TrackGroup} as input)
 * 
 * @author Stephane Dallongeville
 */
public class ROITrackStatistics extends Plugin implements ROIBlock, PluginBundled
{
    /**
     * Build the plot describing the {@link ROIDescriptor} evolution along given input tracks
     * 
     * @throws InterruptedException
     * @throws UnsupportedOperationException
     */
    public static void buildPlot(List<TrackGroup> trackGroups, Sequence sequence, int channel, ROIDescriptor descriptor, Plot2DPanel plotPanel)
            throws UnsupportedOperationException, InterruptedException
    {
        final int sizeZ = (sequence != null) ? sequence.getSizeZ() : 1;
        final double timeInterval = (sequence != null) ? sequence.getTimeInterval() : 1d;

        final String name = descriptor.getName();
        final String unit = descriptor.getUnit(sequence);
        final String title = name + " (" + (unit == null ? "a.u." : unit) + ")";

        plotPanel.removeAllPlots();
        plotPanel.removeAllPlotables();
        plotPanel.setAxisLabels("Time (sec.)", title);

        for (TrackGroup tg : trackGroups)
        {
            final List<TrackSegment> tracks = tg.getTrackSegmentList();

            for (int trackInd = 0; trackInd < tracks.size(); trackInd++)
            {
                final TrackSegment ts = tracks.get(trackInd);

                if (!ts.isAllDetectionEnabled())
                    continue;

                final List<Detection> detections = ts.getDetectionList();
                final double[][] xy = new double[detections.size()][];

                for (int detInd = 0; detInd < detections.size(); detInd++)
                {
                    final Detection det = detections.get(detInd);
                    ROI roi = null;

                    if ((det instanceof DetectionSpotTrack) || (det instanceof SpotTrack))
                    {
                        final List<Point3D> pts;

                        if (det instanceof DetectionSpotTrack)
                        {
                            if (((DetectionSpotTrack) det).spot != null)
                                pts = ((DetectionSpotTrack) det).spot.point3DList;
                            else
                                pts = null;
                        }
                        else
                            pts = ((SpotTrack) det).getPoint3DList();

                        // several points ?
                        if ((pts != null) && (pts.size() > 1))
                        {
                            // 2D ROI ?
                            if (sizeZ == 1)
                            {
                                roi = new ROI2DArea();
                                roi.beginUpdate();
                                try
                                {
                                    for (Point3D pt : pts)
                                        ((ROI2DArea) roi).addPoint((int) pt.x, (int) pt.y);
                                }
                                finally
                                {
                                    roi.endUpdate();
                                }
                            }
                            // 3D ROI
                            else
                            {
                                roi = new ROI3DArea();
                                roi.beginUpdate();
                                try
                                {
                                    for (Point3D pt : pts)
                                        ((ROI3DArea) roi).addPoint((int) pt.x, (int) pt.y, (int) pt.z);
                                }
                                finally
                                {
                                    roi.endUpdate();
                                }
                            }
                        }
                    }
                    else if (det instanceof ActiveContour)
                    {
                        roi = ((ActiveContour) det).toROI(ROIType.AREA, sequence);
                    }

                    // use detection position to define the ROI
                    if (roi == null)
                    {
                        if (sizeZ == 1)
                            roi = new ROI2DPoint(det.getX(), det.getY());
                        else
                            roi = new ROI3DPoint(det.getX(), det.getY(), det.getZ());
                    }

                    if (descriptor.separateChannel() && (channel != -1))
                        roi = roi.getSubROI(-1, -1, channel);

                    // set ROI T position from detection T position
                    if (roi instanceof ROI2D)
                        ((ROI2D) roi).setT(det.getT());
                    else if (roi instanceof ROI3D)
                        ((ROI3D) roi).setT(det.getT());

                    if (roi.isEmpty())
                    {
                        xy[detInd] = new double[] {det.getT() * timeInterval, 0d};
                    }
                    else
                    {
                        double value = 0d;
                        final Object res = descriptor.compute(roi, sequence);

                        if (res instanceof Double)
                        {
                            final double d = ((Double) res).doubleValue();

                            if (Double.isFinite(d))
                                value = d;
                        }

                        xy[detInd] = new double[] {det.getT() * timeInterval, value};
                    }
                }

                plotPanel.addLinePlot(tg.getDescription() + " #" + trackInd, ts.getFirstDetection().getColor(), xy);
            }
        }
    }

    /**
     * Build and return the workbook describing the {@link ROIDescriptor} evolution along given input tracks
     */
    public static Workbook getWorkBook(Sequence sequence, String name, Plot2DPanel plotPanel)
    {
        final Workbook wb = Workbooks.createEmptyWorkbook();
        final IcySpreadSheet sheet = Workbooks.getSheet(wb, name);

        final int sizeT = (sequence != null) ? sequence.getSizeT() : 1;
        final double tScale = (sequence != null) ? sequence.getTimeInterval() : 1d;

        // write the time column
        sheet.setValue(0, 0, "Time (sec.)");

        for (int t = 0; t < sizeT; t++)
            sheet.setValue(t + 1, 0, Double.valueOf(t * tScale));

        int column = 1;
        for (Plot plot : plotPanel.getPlots())
        {
            final double[][] data = plot.getData();

            sheet.setValue(0, column, plot.getName());
            for (double[] xy : data)
            {
                // retrieve the row from the time step
                final int row = 1 + (int) Math.round(xy[0] / tScale);
                sheet.setValue(row, column, Double.valueOf(xy[1]));
            }
            column++;
        }

        return wb;
    }

    // VAR
    public final Var<TrackGroup> tracks;
    public final VarSequence sequence;
    public final VarInteger channel;
    public final VarROIDescriptor descriptor;
    public final VarWorkbook workbook;

    public ROITrackStatistics()
    {
        super();

        tracks = new Var<TrackGroup>("Track group", new TrackGroup(null));
        sequence = new VarSequence("Sequence", null);
        channel = new VarInteger("Channel", 0);
        descriptor = new VarROIDescriptor("Descriptor");
        workbook = new VarWorkbook("Workbook", Workbooks.createEmptyWorkbook());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("trackgroup", tracks);
        inputMap.add("sequence", sequence);
        inputMap.add("channel", channel);
        inputMap.add("descriptor", descriptor);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("workbook", workbook);
    }

    @Override
    public void run()
    {
        // execution from protocol
        final TrackGroup trackGroup = tracks.getValue();
        final Sequence seq = sequence.getValue();
        final ROIDescriptor desc = ROIDescriptor.getDescriptor(descriptor.getValue());
        final int ch = ((desc != null) && desc.separateChannel()) ? channel.getValue().intValue() : -1;

        if ((seq != null) && (ch >= seq.getSizeC()))
            throw new VarException(channel, "Invalid channel parameter (cannot be > " + (seq.getSizeC() - 1) + ")");

        try
        {
            if ((trackGroup != null) && (desc != null))
            {
                final Plot2DPanel plotPanel = new Plot2DPanel();

                // start by building plot
                ROITrackStatistics.buildPlot(CollectionUtil.createArrayList(trackGroup), seq, ch, desc, plotPanel);
                // then retrieve workbook from the plot
                final Workbook wb = getWorkBook(seq, desc.getName(), plotPanel);

                // set output value
                workbook.setValue(wb);
            }
        }
        catch (UnsupportedOperationException e)
        {
            throw new VarException(descriptor, e.getMessage());
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
}
