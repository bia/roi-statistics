package plugins.adufour.roi;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

import icy.file.FileUtil;
import icy.gui.main.GlobalSequenceListener;
import icy.main.Icy;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import icy.preferences.XMLPreferences;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIDescriptor;
import icy.roi.ROIEvent;
import icy.roi.ROIEvent.ROIEventType;
import icy.roi.ROIListener;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.sequence.SequenceListener;
import icy.system.IcyExceptionHandler;
import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.system.thread.ThreadUtil;
import icy.type.point.Point3D;
import icy.type.point.Point5D;
import icy.type.rectangle.Rectangle5D;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzDialog;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.roi.ROIConvexHullDescriptor.ROIConvexity;
import plugins.adufour.roi.ROIEllipsoidFittingDescriptor.ROIElongation;
import plugins.adufour.roi.ROIEllipsoidFittingDescriptor.ROIFlatness3D;
import plugins.adufour.roi.ROIEllipsoidFittingDescriptor.ROIPitchAngle;
import plugins.adufour.roi.ROIEllipsoidFittingDescriptor.ROIRollAngle;
import plugins.adufour.roi.ROIEllipsoidFittingDescriptor.ROIYawAngle;
import plugins.adufour.roi.ROIFeretDiameterDescriptor.ROIFeretDiameter;
import plugins.adufour.roi.ROIRoundnessDescriptor.ROIRoundness;
import plugins.adufour.roi.ROISphericityDescriptor.ROISphericity;
import plugins.adufour.roi.intensitycenter.ROIIntensityCenterDescriptorsPlugin;
import plugins.adufour.roi.intensitycenter.ROIIntensityCenterXDescriptor;
import plugins.adufour.roi.intensitycenter.ROIIntensityCenterYDescriptor;
import plugins.adufour.roi.intensitycenter.ROIIntensityCenterZDescriptor;
import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.gui.swing.WorkbookEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarLong;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.vars.util.VarListener;
import plugins.adufour.vars.util.VarReferencingPolicy;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.kernel.roi.descriptor.measure.ROIBasicMeasureDescriptorsPlugin;
import plugins.kernel.roi.descriptor.measure.ROIContourDescriptor;
import plugins.kernel.roi.descriptor.measure.ROIInteriorDescriptor;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;

/**
 * Basic measures on regions of interest
 * 
 * @author Alexandre Dufour
 * @author Daniel Gonzalez
 */
public class ROIMeasures extends EzPlug implements ROIBlock, GlobalSequenceListener, SequenceListener, ROIListener
{

    /**
     * Measures available for this plugin.
     * <b>Note:</b> The id is used when loading and saving parameters.
     * Please take this into account when adding new descriptors. The maximum usable amount of descriptors is 64,
     * since the parameters are saved as a long value (64 bits).
     */
    public enum Measures
    {
        FULLPATH("Full path", 0, null), FOLDER("Folder", 1, null), DATASET("Dataset", 2, null), NAME("Name", 3, null),
        COLOR("Color", 4, null), POSITION_X("Position X", 43, "px"), POSITION_Y("Position Y", 44, "px"),
        POSITION_Z("Position Z", 45, "px"), POSITION_T("Position T", 46, null), POSITION_C("Position C", 47, null),
        SIZE_X("Size X", 13, "px"), SIZE_Y("Size Y", 14, "px"), SIZE_Z("Size Z", 15, "px"), SIZE_T("Size T", 48, null),
        SIZE_C("Size C", 49, null), X_CENTER("Center X", 5, "px"), Y_CENTER("Center Y", 6, "px"),
        Z_CENTER("Center Z", 7, "px"), T_CENTER("Center T", 8, null), C_CENTER("Center C", 9, null),
        X_GLOBAL_CENTER("Global center X", 10, "px"), Y_GLOBAL_CENTER("Global center Y", 11, "px"),
        Z_GLOBAL_CENTER("Global center Z", 12, "px"), CONTOUR("Contour", 16, "px"), INTERIOR("Interior", 17, "px"),
        SPHERICITY("Sphericity", 18, null), ROUNDNESS("Roundness", 19, "%"), CONVEXITY("Convexity", 20, "%"),
        MAX_FERET("Max Feret diameter", 21, "um"), ELLIPSE_A("1st Diameter", 22, "um"),
        ELLIPSE_B("2nd Diameter", 23, "um"), ELLIPSE_C("3rd Diameter", 24, "um"), YAW("Yaw", 25, "°"),
        PITCH("Pitch", 26, "°"), ROLL("Roll", 27, "°"), ELONGATION("Elongation", 28, null),
        FLATNESS3D("Flatness3D", 29, null), INTENSITY_MIN("Min Intensity", 30, null),
        INTENSITY_AVG("Mean Intensity", 31, null), INTENSITY_MAX("Max Intensity", 32, null),
        INTENSITY_SUM("Sum Intensity", 33, null), INTENSITY_STD("Standard Deviation", 34, null),
        INTENSITY_TEXTURE_ASM("Texture angular second moment", 35, null),
        INTENSITY_TEXTURE_CONT("Texture contrast", 36, null), INTENSITY_TEXTURE_ENT("Texture entropy", 37, null),
        INTENSITY_TEXTURE_HOMO("Texture homogeneity", 38, null), PERIMETER("Perimeter", 39, "um"),
        AREA("Area", 40, "um2"), SURFACE_AREA("Surface Area", 41, "um2"), VOLUME("Volume", 42, "um3"),
        INTENSITY_X(ROIIntensityCenterXDescriptor.ID, 50, "px"),
        INTENSITY_Y(ROIIntensityCenterYDescriptor.ID, 51, "px"),
        INTENSITY_Z(ROIIntensityCenterZDescriptor.ID, 52, "px");

        final String name;
        final int id;
        final String unit;

        Measures(String name, int id, String unit)
        {
            this.name = name;
            this.id = id;
            this.unit = unit;
        }

        boolean selected = true;

        public void toggleSelection()
        {
            selected = !selected;
        }

        public void setSelected(boolean value)
        {
            selected = value;
        }

        @Override
        public String toString()
        {
            return name;
        }

        public String toHeader()
        {
            return name + (hasUnit() ? " (" + getUnit() + ")" : "");
        }

        public boolean hasUnit()
        {
            return unit != null && !unit.isEmpty();
        }

        public String getUnit()
        {
            return unit;
        }

        public boolean isSelected()
        {
            return selected;
        }

        public int getColumnIndex()
        {
            if (!isSelected())
                return -1;

            Measures[] allMeasures = values();
            int deselectedMeasures = 0;
            for (int i = 0; i < allMeasures.length; i++)
            {
                if (allMeasures[i] == this)
                    return i - deselectedMeasures;
                if (!allMeasures[i].isSelected())
                    deselectedMeasures++;
            }

            // should never happen
            return -1;
        }

        public static Measures getMeasureById(int id)
        {
            return getOrderedById()[id];
        }

        private static Measures[] orderedById;

        public static Measures[] getOrderedById()
        {
            if (orderedById == null)
            {
                orderMeasures();
            }
            return orderedById;
        }

        private static void orderMeasures()
        {
            List<Measures> orderedMeasures = Arrays.asList(Measures.values());
            Collections.sort(orderedMeasures, new Comparator<Measures>()
            {
                @Override
                public int compare(Measures o1, Measures o2)
                {
                    return Integer.compare(o1.id, o2.id);
                }
            });
            orderedById = orderedMeasures.toArray(new Measures[0]);
        }
    }

    // need a bit waiting queue as we send one task per ROI !
    Processor cpus = new Processor(2 * 1024 * 1024, SystemUtil.getNumberOfCPUs());
    final VarMeasureSelector measureSelector;
    final VarROIArray rois = new VarROIArray("Regions of interest");
    final VarWorkbook book = new VarWorkbook("Workbook", (Workbook) null);
    final VarSequence sequence = new VarSequence("Sequence", null);

    boolean measureSelectionChanged = false;

    public ROIMeasures()
    {
        measureSelector = new VarMeasureSelector(new MeasureSelector());

        // use XML plugin preferences to initialize variable value
        final XMLPreferences prefs = getPreferences("measures");

        long value = 0;
        int i = 0;
        for (Measures measure : Measures.values())
        {
            measure.selected = prefs.node(measure.name()).getBoolean("selected", true);
            if ((i < 64) && measure.selected)
                value |= 1L << i;
            i++;
        }

        // so we correctly restore variable value from XML preferences
        // (we need to use the variable value to correctly load / save selected features
        // with Protocols)
        measureSelector.setValue(Long.valueOf(value));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        measureSelector.setReferencingPolicy(VarReferencingPolicy.NONE);
        inputMap.add("measures", measureSelector);
        inputMap.add("Regions of interest", rois);
        inputMap.add("Sequence", sequence);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Workbook", book);
    }

    @Override
    protected void initialize()
    {
        getUI().setActionPanelVisible(false);

        addComponent((JComponent) measureSelector.createVarEditor(true).getEditorComponent());

        WorkbookEditor viewer = new WorkbookEditor(book);
        viewer.setReadOnly(true);
        viewer.setEnabled(true);
        viewer.setFirstRowAsHeader(true);
        viewer.setOpenButtonVisible(false);
        JComponent jc = viewer.getEditorComponent();
        jc.setPreferredSize(new Dimension(400, 300));
        addComponent(jc);

        if (!isHeadLess())
            Icy.getMainInterface().addGlobalSequenceListener(this);

        getUI().clickRun();
    }

    @Override
    public void execute()
    {
        Workbook wb = book.getValue();

        // Create the workbook anew if:
        // - it did not exist
        // - we are in block mode (forget previous results)
        if (wb == null || isHeadLess())
        {
            book.setValue(wb = Workbooks.createEmptyWorkbook());
        }

        if (!isHeadLess())
            for (Sequence attachedSequence : getSequences())
            {
                attachedSequence.addListener(this);
                for (ROI roi : attachedSequence.getROIs())
                    roi.addListener(this);
            }

        updateStatistics();
    }

    private String getDataSetName(Sequence sequenceOfInterest)
    {
        String dataSetName = "ROI Statistics";

        if (sequenceOfInterest == null)
        {
            // try retrieving the sequence attached to the first ROI
            List<Sequence> sequences = rois.getValue()[0].getSequences();
            if (sequences.size() > 0)
                sequenceOfInterest = sequences.get(0);
        }

        if (sequenceOfInterest == null)
        {
            // no hope...
            dataSetName = "--";
        }
        else
        {
            // replace the sheet name by the file or sequence name
            dataSetName = FileUtil.getFileName(sequenceOfInterest.getFilename());
            if (StringUtil.isEmpty(dataSetName))
                dataSetName = sequenceOfInterest.getName();
        }

        // make the name "safe"
        return dataSetName;
    }

    String getSheetName(Sequence sequenceOfInterest)
    {
        String sheetName = "ROI Statistics";
        if (sequenceOfInterest != null && !isHeadLess())
            sheetName = getDataSetName(sequenceOfInterest);
        return WorkbookUtil.createSafeSheetName(sheetName);
    }

    private void updateStatistics()
    {
        if (isHeadLess())
        {
            updateStatistics(sequence.getValue());
        }
        else
            for (Sequence sequenceOfInterest : getSequences())
            {
                updateStatistics(sequenceOfInterest);
            }
    }

    private void updateStatistics(final Sequence sequenceOfInterest)
    {
        // don't compute statistics if specified sequence is null
        // if (sequenceOfInterest == null)
        // return;

        final Workbook wb = book.getValue();

        String sheetName = getSheetName(sequenceOfInterest);

        final Sheet sheet;
        Row header = null;

        if (wb.getSheet(sheetName) != null)
        {
            sheet = wb.getSheet(sheetName);
        }
        else
        {
            // create the sheet
            sheet = wb.createSheet(sheetName);
            measureSelectionChanged = true;
        }

        if (measureSelectionChanged)
        {
            measureSelectionChanged = false;
            header = sheet.createRow(0);

            // create the header row
            Measures[] measures = Measures.values();
            final int sizeC = (sequenceOfInterest != null) ? sequenceOfInterest.getSizeC() : 0;

            int col = 0;
            for (Measures measure : measures)
            {
                if (!measure.isSelected())
                    continue;

                String name = measure.toHeader();

                switch (measure)
                {
                    case INTENSITY_MIN:
                    case INTENSITY_MAX:
                    case INTENSITY_SUM:
                    case INTENSITY_AVG:
                    case INTENSITY_STD:
                    case INTENSITY_TEXTURE_ASM:
                    case INTENSITY_TEXTURE_CONT:
                    case INTENSITY_TEXTURE_ENT:
                    case INTENSITY_TEXTURE_HOMO:
                    case INTENSITY_X:
                    case INTENSITY_Y:
                    case INTENSITY_Z:
                        for (int c = 0; c < sizeC; c++)
                        {
                            header.getCell(col).setCellValue(name + " (" + sequenceOfInterest.getChannelName(c) + ")");
                            col++;
                        }
                        break;

                    default:
                        header.getCell(col).setCellValue(name);
                        col++;
                        break;
                }
            }
        }

        final List<ROI> rois2Update = (isHeadLess() || (sequenceOfInterest == null))
                ? Arrays.asList(this.rois.getValue())
                : sequenceOfInterest.getROIs(true);

        Runnable fullUpdate = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    IcySpreadSheet icySheet = new IcySpreadSheet(sheet);

                    int rowID = 1;
                    icySheet.removeRows(rowID);

                    List<Future<List<Object>>> results = new ArrayList<Future<List<Object>>>(rois2Update.size());

                    for (ROI roi : rois2Update)
                    {
                        if (cpus.isShutdown() || cpus.isTerminating())
                            return;
                        results.add(cpus.submit(createUpdater(sequenceOfInterest, roi)));
                    }

                    for (Future<List<Object>> result : results)
                        updateWorkbook(wb, icySheet, rowID++, result.get(), false);
                }
                catch (InterruptedException e)
                {
                    Thread.currentThread().interrupt();
                    return;
                }
                catch (Exception e)
                {
                    IcyExceptionHandler.showErrorMessage(e, true);
                    return;
                }

                book.valueChanged(book, null, book.getValue());
            }
        };

        if (isHeadLess())
        {
            fullUpdate.run();
        }
        else
        {
            ThreadUtil.bgRun(fullUpdate);
        }
    }

    private void updateStatistics(ROI roi)
    {
        Workbook wb = book.getValue();

        for (Sequence sequenceOfInterest : roi.getSequences())
            if (Icy.getMainInterface().isOpened(sequenceOfInterest))
                try
                {
                    IcySpreadSheet sheet = Workbooks.getSheet(wb, getSheetName(sequenceOfInterest));

                    int rowID = sequenceOfInterest.getROIs(true).indexOf(roi) + 1;
                    {
                        List<Object> measures = createUpdater(sequenceOfInterest, roi).call();
                        updateWorkbook(wb, sheet, rowID, measures, true);
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
    }

    private static String colorToString(Color color)
    {
        return (StringUtil.toHexaString(color.getAlpha(), 2) + StringUtil.toHexaString(color.getRed(), 2)
                + StringUtil.toHexaString(color.getGreen(), 2) + StringUtil.toHexaString(color.getBlue(), 2))
                        .toUpperCase();
    }

    private void updateWorkbook(Workbook wb, IcySpreadSheet sheet, int rowID, List<Object> measures,
            boolean updateInterface)
    {
        for (int colID = 0; colID < measures.size(); colID++)
        {
            Object value = measures.get(colID);

            if (value instanceof Color)
            {
                sheet.setFillColor(rowID, colID, (Color) value);
                sheet.setValue(rowID, colID, colorToString((Color) value));
            }
            else
                sheet.setValue(rowID, colID, value);
        }

        if (updateInterface)
            book.valueChanged(book, null, book.getValue());
    }

    private Callable<List<Object>> createUpdater(final Sequence sequenceOfInterest, final ROI roi2Update)
    {
        return new Callable<List<Object>>()
        {
            @Override
            public List<Object> call() throws InterruptedException
            {
                final List<Object> measures = new ArrayList<Object>();
                final List<Measures> measuresToCalculate = new ArrayList<>();

                for (Measures m : Measures.values())
                    if (m.isSelected())
                        measuresToCalculate.add(m);

                ROI roi = roi2Update;
                final int sizeC = (sequenceOfInterest != null) ? sequenceOfInterest.getSizeC() : 0;

                // Check bundled descriptors and pre-compute values
                Point5D center = null;
                Point3D globalCenter = null;
                if (Measures.X_CENTER.isSelected() || Measures.Y_CENTER.isSelected() || Measures.Z_CENTER.isSelected()
                        || Measures.T_CENTER.isSelected() || Measures.C_CENTER.isSelected()
                        || Measures.X_GLOBAL_CENTER.isSelected() || Measures.Y_GLOBAL_CENTER.isSelected()
                        || Measures.Z_GLOBAL_CENTER.isSelected())
                {
                    center = ROIMassCenterDescriptorsPlugin.computeMassCenter(roi);
                    globalCenter = center.toPoint3D();
                    if (sequenceOfInterest != null)
                    {
                        globalCenter.setX(sequenceOfInterest.getPositionX()
                                + globalCenter.getX() * sequenceOfInterest.getPixelSizeX());
                        globalCenter.setY(sequenceOfInterest.getPositionY()
                                + globalCenter.getY() * sequenceOfInterest.getPixelSizeY());
                        globalCenter.setZ(sequenceOfInterest.getPositionZ()
                                + globalCenter.getZ() * sequenceOfInterest.getPixelSizeZ());
                    }
                }

                Rectangle5D bounds5 = null;
                if (Measures.POSITION_X.isSelected() || Measures.POSITION_Y.isSelected()
                        || Measures.POSITION_Z.isSelected() || Measures.POSITION_T.isSelected()
                        || Measures.POSITION_C.isSelected() || Measures.SIZE_X.isSelected()
                        || Measures.SIZE_Y.isSelected() || Measures.SIZE_Z.isSelected() || Measures.SIZE_T.isSelected()
                        || Measures.SIZE_C.isSelected())
                {
                    bounds5 = roi.getBounds5D();
                }

                double[] ellipsoidValues = null;
                if (Measures.ELLIPSE_A.isSelected() || Measures.ELLIPSE_B.isSelected()
                        || Measures.ELLIPSE_C.isSelected() || Measures.YAW.isSelected() || Measures.PITCH.isSelected()
                        || Measures.ROLL.isSelected() || Measures.ELONGATION.isSelected()
                        || Measures.FLATNESS3D.isSelected())
                {
                    ellipsoidValues = ROIEllipsoidFittingDescriptor.computeOrientation(roi, sequenceOfInterest);
                }

                double[] channelMins = null;
                double[] channelMaxs = null;
                double[] channelSums = null;
                int[] channelPointCounts = null;
                double[] channelAvgs = null;
                double[] channelStdDevs = null;

                if (Measures.INTENSITY_MIN.isSelected() || Measures.INTENSITY_AVG.isSelected()
                        || Measures.INTENSITY_MAX.isSelected() || Measures.INTENSITY_SUM.isSelected()
                        || Measures.INTENSITY_STD.isSelected())
                {

                    channelMins = new double[sizeC];
                    channelMaxs = new double[sizeC];
                    channelSums = new double[sizeC];
                    channelPointCounts = new int[sizeC];
                    channelAvgs = new double[sizeC];
                    channelStdDevs = new double[sizeC];

                    for (int c = 0; c < sizeC; c++)
                    {
                        if (Thread.currentThread().isInterrupted())
                            return measures;

                        // we want intensity measures for intersecting points as well
                        SequenceDataIterator iterator = new SequenceDataIterator(sequenceOfInterest, roi, true, -1, -1,
                                c);

                        // minimum / maximum / sum
                        channelMins[c] = Double.MAX_VALUE;
                        channelMaxs[c] = 0;
                        channelSums[c] = 0;
                        channelPointCounts[c] = 0;
                        while (!iterator.done())
                        {
                            double val = iterator.get();
                            if (val > channelMaxs[c])
                                channelMaxs[c] = val;
                            if (val < channelMins[c])
                                channelMins[c] = val;
                            channelSums[c] += val;
                            channelPointCounts[c]++;
                            iterator.next();
                        }
                        channelAvgs[c] = channelSums[c] / channelPointCounts[c];

                        if (Measures.INTENSITY_STD.isSelected())
                        {
                            channelStdDevs[c] = 0;
                            iterator.reset();
                            while (!iterator.done())
                            {
                                double dev = iterator.get() - channelAvgs[c];
                                channelStdDevs[c] += dev * dev;
                                iterator.next();
                            }
                            channelStdDevs[c] = Math.sqrt(channelStdDevs[c] / channelPointCounts[c]);
                        }
                    }
                }

                Map<ROIDescriptor, Object>[] haralickTextureValues = null;
                Set<String> haralickErrors = new HashSet<>();
                if (Measures.INTENSITY_TEXTURE_ASM.isSelected() || Measures.INTENSITY_TEXTURE_CONT.isSelected()
                        || Measures.INTENSITY_TEXTURE_ENT.isSelected() || Measures.INTENSITY_TEXTURE_HOMO.isSelected())
                {
                    @SuppressWarnings({"unchecked"})
                    Map<ROIDescriptor, Object>[] valueMap = new Map[sizeC];
                    haralickTextureValues = valueMap;
                    if (roi instanceof ROI2D)
                    {
                        ROI2D rCopy = (ROI2D) roi.getCopy();
                        for (int c = 0; c < sizeC; c++)
                        {
                            try
                            {
                                rCopy.setC(c);
                                haralickTextureValues[c] = new ROIHaralickTextureDescriptor().compute(rCopy,
                                        sequenceOfInterest);
                            }
                            catch (UnsupportedOperationException ex)
                            {
                                final String mess = ex.getMessage();

                                // we don't want to spam output with these errors
                                if (!haralickErrors.contains(mess))
                                {
                                    haralickErrors.add(mess);
                                    System.err.println(mess);
                                }
                            }
                        }
                    }
                }

                Point3D[] intensityCenters = null;
                if (Measures.INTENSITY_X.isSelected() || Measures.INTENSITY_Y.isSelected()
                        || Measures.INTENSITY_Z.isSelected())
                {
                    intensityCenters = new Point3D[sizeC];
                    ROI rCopy = roi.getCopy();
                    Point5D rPosition = rCopy.getPosition5D();
                    for (int c = 0; c < sizeC; c++)
                    {
                        rPosition.setC(c);
                        rCopy.setPosition5D(rPosition);
                        intensityCenters[c] = ROIIntensityCenterDescriptorsPlugin.computeIntensityCenter(rCopy,
                                sequenceOfInterest);
                    }
                }

                // Add measure results in the correct order
                for (Measures m : measuresToCalculate)
                {
                    if (Thread.currentThread().isInterrupted())
                        return measures;

                    try
                    {
                        switch (m)
                        {
                            // Metadata descriptors
                            case FULLPATH:
                            case FOLDER:
                                String path = "";
                                if (sequenceOfInterest != null && sequenceOfInterest.getFilename() != null)
                                    path = sequenceOfInterest.getFilename();

                                if (m == Measures.FULLPATH)
                                {
                                    if (StringUtil.isEmpty(path))
                                        measures.add("---");
                                    else
                                        measures.add(FileUtil.getDirectory(path, false));
                                }

                                if (m == Measures.FOLDER)
                                {
                                    if (StringUtil.isEmpty(path))
                                        measures.add("---");
                                    else
                                        measures.add(FileUtil.getFileName(FileUtil.getDirectory(path, false)));
                                }
                                break;
                            case DATASET:
                                measures.add(getDataSetName(sequenceOfInterest));
                                break;
                            case NAME:
                                measures.add(roi.getName());
                                break;
                            case COLOR:
                                measures.add(roi.getColor());
                                break;

                            // Position descriptors (MASS CENTER)
                            case X_CENTER:
                                measures.add(center.getX());
                                break;
                            case Y_CENTER:
                                measures.add(center.getY());
                                break;
                            case Z_CENTER:
                                measures.add(roi instanceof ROI2D && center.getZ() == -1 ? "ALL" : center.getZ());
                                break;
                            case T_CENTER:
                                measures.add(center.getT() == -1 ? "ALL" : center.getT());
                                break;
                            case C_CENTER:
                                measures.add(center.getC() == -1 ? "ALL" : center.getC());
                                break;
                            case X_GLOBAL_CENTER:
                                measures.add(globalCenter.getX());
                                break;
                            case Y_GLOBAL_CENTER:
                                measures.add(globalCenter.getY());
                                break;
                            case Z_GLOBAL_CENTER:
                                measures.add(roi instanceof ROI2D && center.getZ() == -1 ? "ALL" : globalCenter.getZ());
                                break;

                            // Bound descriptors
                            case POSITION_X:
                                measures.add(bounds5.getX());
                                break;
                            case POSITION_Y:
                                measures.add(bounds5.getY());
                                break;
                            case POSITION_Z:
                                if (Double.isInfinite(bounds5.getZ()))
                                {
                                    measures.add("ALL");
                                }
                                else
                                {
                                    measures.add(bounds5.getZ());
                                }
                                break;
                            case POSITION_T:
                                if (Double.isInfinite(bounds5.getT()))
                                {
                                    measures.add("ALL");
                                }
                                else
                                {
                                    measures.add(bounds5.getT());
                                }
                                break;
                            case POSITION_C:
                                if (Double.isInfinite(bounds5.getC()))
                                {
                                    measures.add("ALL");
                                }
                                else
                                {
                                    measures.add(bounds5.getC());
                                }
                                break;
                            case SIZE_X:
                                measures.add(bounds5.getSizeX());
                                break;
                            case SIZE_Y:
                                measures.add(bounds5.getSizeY());
                                break;
                            case SIZE_Z:
                                if (roi instanceof ROI3D)
                                    measures.add(bounds5.getSizeZ());
                                else if (roi instanceof ROI2D)
                                    if (Double.isInfinite(bounds5.getSizeZ()))
                                    {
                                        measures.add("ALL");
                                    }
                                    else
                                    {
                                        measures.add(1);
                                    }
                                else
                                    throw new RuntimeException("Not a ROI3D");
                                break;
                            case SIZE_T:
                                if (Double.isInfinite(bounds5.getSizeT()))
                                {
                                    measures.add("ALL");
                                }
                                else
                                {
                                    measures.add(bounds5.getSizeT());
                                }
                                break;
                            case SIZE_C:
                                if (Double.isInfinite(bounds5.getSizeC()))
                                {
                                    measures.add("ALL");
                                }
                                else
                                {
                                    measures.add(bounds5.getSizeC());
                                }
                                break;

                            // Contour and interior descriptors in pixels
                            case CONTOUR:
                                measures.add(ROIContourDescriptor.computeContour(roi));
                                break;
                            case INTERIOR:
                                measures.add(ROIInteriorDescriptor.computeInterior(roi));
                                break;

                            // Shape descriptors
                            case SPHERICITY:
                                measures.add(ROISphericity.computeSphericity(roi));
                                break;
                            case ROUNDNESS:
                                measures.add((roi.getNumberOfPoints() == 1) ? 100 : ROIRoundness.computeRoundness(roi));
                                break;
                            case CONVEXITY:
                                measures.add(ROIConvexity.computeConvexity(roi));
                                break;
                            case MAX_FERET:
                                measures.add(ROIFeretDiameter.computeFeretDiameter(roi, sequenceOfInterest));
                                break;

                            // Ellipse descriptors
                            case ELLIPSE_A:
                                measures.add(ellipsoidValues[0]);
                                break;
                            case ELLIPSE_B:
                                measures.add(ellipsoidValues[1]);
                                break;
                            case ELLIPSE_C:
                                if (roi instanceof ROI3D)
                                    measures.add(ellipsoidValues[2]);
                                else
                                    throw new RuntimeException("Not a ROI3D");
                                break;
                            case YAW:
                                measures.add(ROIYawAngle.computeYawAngle(ellipsoidValues));
                                break;
                            case PITCH:
                                measures.add(ROIPitchAngle.computePitchAngle(ellipsoidValues));
                                break;
                            case ROLL:
                                measures.add(ROIRollAngle.computeRollAngle(ellipsoidValues));
                                break;
                            case ELONGATION:
                                if (ellipsoidValues[1] != 0)
                                {
                                    measures.add(ROIElongation.computeElongation(ellipsoidValues));
                                }
                                else
                                    throw new RuntimeException("Zero elongation");
                                break;
                            case FLATNESS3D:
                                if (roi instanceof ROI3D && ellipsoidValues[2] != 0)
                                {
                                    measures.add(ROIFlatness3D.computeFlatness3D(ellipsoidValues));
                                }
                                else
                                    throw new RuntimeException("Not a ROI3D or zero flatness");
                                break;

                            // Intensity descriptors
                            case INTENSITY_MIN:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(channelMins[c]);
                                }
                                break;
                            case INTENSITY_AVG:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(channelAvgs[c]);
                                }
                                break;
                            case INTENSITY_MAX:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(channelMaxs[c]);
                                }
                                break;
                            case INTENSITY_SUM:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(channelSums[c]);
                                }
                                break;
                            case INTENSITY_STD:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(channelStdDevs[c]);
                                }
                                break;

                            // Texture descriptors
                            case INTENSITY_TEXTURE_ASM:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(haralickTextureValues[c]
                                            .get(ROIHaralickTextureDescriptor.angularSecondMoment));
                                }
                                break;
                            case INTENSITY_TEXTURE_CONT:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(haralickTextureValues[c].get(ROIHaralickTextureDescriptor.contrast));
                                }
                                break;
                            case INTENSITY_TEXTURE_ENT:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(haralickTextureValues[c].get(ROIHaralickTextureDescriptor.entropy));
                                }
                                break;
                            case INTENSITY_TEXTURE_HOMO:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(
                                            haralickTextureValues[c].get(ROIHaralickTextureDescriptor.homogeneity));
                                }
                                break;
                            // Note: Don't use ROI descriptors for computing perimeter!: They are dumb (unit is not fixed)
                            case PERIMETER:
                                measures.add((sequenceOfInterest != null) ? roi.getLength(sequenceOfInterest) : "NA");
                                break;
                            // Note: Don't use ROI descriptors for computing area!: They are dumb (unit is not fixed)
                            case AREA:
                                if (sequenceOfInterest == null)
                                    measures.add("NA");
                                else
                                {
                                    final double mul = ROIBasicMeasureDescriptorsPlugin
                                            .getMultiplierFactor(sequenceOfInterest, roi, 2);

                                    // 0 means the operation is not supported for this ROI
                                    if (mul == 0d)
                                        throw new UnsupportedOperationException();

                                    // convert number of point in area using internal sequence pixel size (approximation)
                                    measures.add(sequenceOfInterest.calculateSize(roi.getNumberOfPoints() * mul, 2, 2));
                                }
                                break;
                            // Note: Don't use ROI descriptors for computing surface area!: They are dumb (unit is not fixed)
                            case SURFACE_AREA:
                                measures.add(
                                        (sequenceOfInterest != null) ? ((ROI3D) roi).getSurfaceArea(sequenceOfInterest)
                                                : "NA");
                                break;
                            // Note: Don't use ROI descriptors for computing volume!: They are dumb (unit is not fixed)
                            case VOLUME:
                                if (sequenceOfInterest == null)
                                    measures.add("NA");
                                else
                                {
                                    final double mul1 = ROIBasicMeasureDescriptorsPlugin
                                            .getMultiplierFactor(sequenceOfInterest, roi, 3);

                                    // 0 means the operation is not supported for this ROI
                                    if (mul1 == 0d)
                                        throw new UnsupportedOperationException();

                                    // convert number of point in volume using internal sequence pixel size
                                    // (approximation)
                                    measures.add(
                                            sequenceOfInterest.calculateSize(roi.getNumberOfPoints() * mul1, 3, 3));
                                }
                                break;
                            case INTENSITY_X:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(intensityCenters[c].getX());
                                }
                                break;
                            case INTENSITY_Y:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(intensityCenters[c].getY());
                                }
                                break;
                            case INTENSITY_Z:
                                for (int c = 0; c < sizeC; c++)
                                {
                                    measures.add(intensityCenters[c].getZ());
                                }
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        if (e instanceof InterruptedException)
                            return measures;
                        measures.add("NA");
                    }
                }
                return measures;
            }
        };
    }

    /**
     * @param roi
     *        the {@link ROI} we want to compute ellipse dimensions
     * @return An array containing the [min, max] diameters of the best fitting
     *         ellipse for the specified ROI
     * @throws InterruptedException
     *         if thread was interrupted during computation
     * @deprecated This method should not be used directly and will be removed in
     *             future releases in favor of a {@link ROIDescriptor}
     *             implementation
     */
    @Deprecated
    public static double[] computeEllipseDimensions(ROI roi) throws InterruptedException
    {
        double[] ellipse = ROIEllipsoidFittingDescriptor.computeOrientation(roi, null);

        return new double[] {ellipse[0], ellipse[1], ellipse[2]};
    }

    /**
     * The sphericity is the normalised ratio between the perimeter and area of a
     * ROI (in 2D), or its volume and surface area (in 3D).
     * 
     * @param roi
     *        the {@link ROI} we want to compute sphericity
     * @return 100% for a perfect circle (or sphere), and lower otherwise
     * @throws InterruptedException
     * @deprecated Use {@link ROISphericity#computeSphericity(ROI)} instead
     */
    @Deprecated
    public static double computeSphericity(ROI roi) throws InterruptedException
    {
        return ROISphericity.computeSphericity(roi);
    }

    /**
     * The roundness is approximated here by the ratio between the smallest and
     * largest distance from the mass center to any point on the surface, and
     * expressed as a percentage.
     * 
     * @param roi
     *        the {@link ROI} we want to compute roundness
     * @return 100% for a perfect circle (or sphere in 3D), and lower otherwise
     * @throws InterruptedException
     *         if thread was interrupted during computation
     * @deprecated Use {@link ROIRoundness#computeRoundness(ROI)} instead
     */
    @Deprecated
    public static double computeRoundness(ROI roi) throws InterruptedException
    {
        return ROIRoundness.computeRoundness(roi);
    }

    /**
     * @param roi
     *        the {@link ROI} we want to compute max feret
     * @return The maximum Feret diameter (i.e. the longest distance between any 2
     *         points of the ROI)
     * @throws InterruptedException
     * @deprecated Use {@link ROIFeretDiameter#computeFeretDiameter(ROI, Sequence)}
     *             instead
     */
    @Deprecated
    public static double computeMaxFeret(ROI roi) throws InterruptedException
    {
        return ROIFeretDiameter.computeFeretDiameter(roi, null);
    }

    /**
     * @param roi
     *        the {@link ROI} we want to compute convexity
     * @return The hull ratio, measured as the ratio between the object volume and
     *         its convex hull (envelope)
     * @throws InterruptedException
     * @deprecated Use {@link ROIConvexity#computeConvexity(ROI)} instead
     */
    @Deprecated
    public static double computeConvexity(ROI roi) throws InterruptedException
    {
        return ROIConvexity.computeConvexity(roi);
    }

    /**
     * @param roi
     *        the {@link ROI} we want to compute Hull dimensions
     * @return An array containing [contour, area] of the smallest convex envelope
     *         surrounding the object. The 2 values are returned together because
     *         their computation is simultaneous
     * @throws InterruptedException
     * @deprecated use {@link Convexify#createConvexROI(ROI)} instead
     */
    @Deprecated
    public static double[] computeHullDimensions(ROI roi) throws InterruptedException
    {
        ROI convexHull = Convexify.createConvexROI(roi);

        return new double[] {convexHull.getNumberOfContourPoints(), convexHull.getNumberOfPoints()};
    }

    // PUBLIC STATIC METHODS (useful for script access) //

    /**
     * @param roi
     *        the {@link ROI} we want to compute mass center
     * @return the mass center as {@link Point5D}
     * @throws InterruptedException
     *         if thread was interrupted during computation
     * @deprecated use {@link ROIMassCenterDescriptorsPlugin#computeMassCenter(ROI)}
     *             instead.
     */
    @Deprecated
    public static Point5D getMassCenter(ROI roi) throws InterruptedException
    {
        return ROIMassCenterDescriptorsPlugin.computeMassCenter(roi);
    }

    /**
     * @param roi
     *        the {@link ROI} we want to compute mass center
     * @return the mass center as {@link Point2D}
     * @throws InterruptedException
     *         if thread was interrupted during computation
     * @deprecated use {@link ROIMassCenterDescriptorsPlugin#computeMassCenter(ROI)}
     *             instead.
     */
    @Deprecated
    public static Point2D getMassCenter(ROI2D roi) throws InterruptedException
    {
        return getMassCenter((ROI) roi).toPoint2D();
    }

    /**
     * @param roi
     *        the {@link ROI} we want to compute mass center
     * @return the mass center as {@link Point3D}
     * @throws InterruptedException
     *         if thread was interrupted during computation
     * @deprecated use {@link ROIMassCenterDescriptorsPlugin#computeMassCenter(ROI)}
     *             instead.
     */
    @Deprecated
    public static Point3D getMassCenter(ROI3D roi) throws InterruptedException
    {
        return getMassCenter((ROI) roi).toPoint3D();
    }

    // MAIN LISTENER //

    @Override
    public void sequenceOpened(Sequence openedSequence)
    {
        openedSequence.addListener(this);
        for (ROI roi : openedSequence.getROIs())
            roi.addListener(this);

        // update(null);
        updateStatistics(openedSequence);

        // this is mandatory since sheet creation cannot be detected
        book.valueChanged(book, null, book.getValue());
    }

    @Override
    public void sequenceClosed(Sequence closedSequence)
    {
        closedSequence.removeListener(this);
        for (ROI roi : closedSequence.getROIs())
            roi.removeListener(this);

        String sheetName = getSheetName(closedSequence);

        int index = book.getValue().getSheetIndex(sheetName);

        if (index >= 0)
        {
            book.getValue().removeSheetAt(index);
            // this is mandatory since sheet creation cannot be detected
            book.valueChanged(book, null, book.getValue());
        }
    }

    // SEQUENCE LISTENER //

    @Override
    public void sequenceChanged(SequenceEvent sequenceEvent)
    {
        if (sequenceEvent.getSourceType() == SequenceEventSourceType.SEQUENCE_DATA)
        {

            updateStatistics(sequenceEvent.getSequence());
        }
        else if (sequenceEvent.getSourceType() == SequenceEventSourceType.SEQUENCE_ROI)
        {
            ROI roi = (ROI) sequenceEvent.getSource();

            switch (sequenceEvent.getType())
            {
                case ADDED:

                    if (roi == null)
                    {
                        // multiple ROI were added
                        updateStatistics();
                    }
                    else
                    {
                        for (Sequence sequenceOfInterest : roi.getSequences())
                            updateStatistics(sequenceOfInterest);
                        // remove it first to avoid duplicates
                        roi.removeListener(this);
                        roi.addListener(this);
                    }
                    break;

                case REMOVED:

                    if (roi == null)
                    {
                        // multiple ROIs have been removed
                        System.err.println("[ROI Statistics] Warning: potential memory leak");
                    }
                    else
                    {
                        roi.removeListener(this);
                    }
                    updateStatistics();
                    break;

                case CHANGED: // don't do anything, roiChanged() will do this for us
            }
        }
    }

    @Override
    public void clean()
    {
        final MeasureSelector selectorGui = measureSelector.gui;
        // release EzDialog (otherwise it remains referenced by IcyFrame.frames)
        if (selectorGui != null)
            selectorGui.selector.close();

        // remove listeners
        for (final Sequence openedSequence : Icy.getMainInterface().getSequences())
            openedSequence.removeListener(this);

        Icy.getMainInterface().removeGlobalSequenceListener(this);

        cpus.shutdownNow();
    }

    @Override
    public void roiChanged(final ROIEvent event)
    {
        if (event.getType() == ROIEventType.ROI_CHANGED
                || event.getType() == ROIEventType.PROPERTY_CHANGED && (event.getPropertyName().equalsIgnoreCase("name")
                        || event.getPropertyName().equalsIgnoreCase("color")))
        {
            updateStatistics(event.getSource());
            // this is mandatory since sheet modification cannot be detected
            book.valueChanged(book, null, book.getValue());
        }
    }

    private static interface VarMeasureSelectorListener extends VarListener<Long>
    {
        public abstract void triggered(VarMeasureSelector source);
    }

    /**
     * Custom Button SwingVarEditor
     * 
     * @author Stephane
     */
    private static class ButtonVarEditor extends SwingVarEditor<Long>
    {
        private ActionListener listener;

        public ButtonVarEditor(VarMeasureSelector variable)
        {
            super(variable);

            setNameVisible(false);
        }

        @Override
        protected JButton createEditorComponent()
        {
            if (getEditorComponent() != null)
                deactivateListeners();

            listener = new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    ((VarMeasureSelector) variable).trigger();
                }
            };

            return new JButton(variable.getName());
        }

        @Override
        public JButton getEditorComponent()
        {
            return (JButton) super.getEditorComponent();
        }

        @Override
        public double getComponentVerticalResizeFactor()
        {
            return 0.0;
        }

        @Override
        protected void activateListeners()
        {
            getEditorComponent().addActionListener(listener);
        }

        @Override
        protected void deactivateListeners()
        {
            getEditorComponent().removeActionListener(listener);
        }

        @Override
        protected void updateInterfaceValue()
        {
            // nothing to do (it's just a button with a name)
        }
    }

    /**
     * Custom VarTrigger for measure selector so we store selected parameters (up to
     * 64) instead of number of trigger inside value
     * 
     * @author Stephane Dallongeville
     */
    private static class VarMeasureSelector extends VarLong
    {
        // GUI (only if no headless)
        final MeasureSelector gui;

        /**
         * Creates a new trigger with the given name
         */
        public VarMeasureSelector(MeasureSelector gui)
        {
            // by default we want to select all
            super("Select features...", -1L);

            this.gui = gui;

            if (gui != null)
                addListener(gui);
        }

        @Override
        public VarEditor<Long> createVarEditor()
        {
            return new ButtonVarEditor(this);
        }

        /**
         * Triggers the variable (equivalent to incrementing the value of this variable)
         */
        public void trigger()
        {
            // fire trigger listeners
            for (VarListener<Long> listener : listeners)
                if (listener instanceof VarMeasureSelectorListener)
                    ((VarMeasureSelectorListener) listener).triggered(this);
        }
    }

    private class MeasureSelector implements VarMeasureSelectorListener
    {
        final EzDialog selector;
        final JCheckBox checkBoxes[];

        public MeasureSelector()
        {
            // we don't need the GUI in headless mode
            if (Icy.getMainInterface().isHeadLess())
            {
                selector = null;
                checkBoxes = null;
            }
            else
            {
                final Measures[] measures = Measures.getOrderedById();

                selector = new EzDialog("Select measures...");
                selector.setLayout(new BoxLayout(selector.getContentPane(), BoxLayout.Y_AXIS));

                checkBoxes = new JCheckBox[measures.length];

                for (int i = 0; i < measures.length; i++)
                {
                    final Measures measure = measures[i];
                    String name = measure.toString();

                    final JCheckBox check = new JCheckBox(name, measure.isSelected());
                    check.addActionListener(new ActionListener()
                    {
                        @Override
                        public void actionPerformed(ActionEvent arg0)
                        {
                            measure.setSelected(check.isSelected());
                            measureSelectionChanged = true;
                        }
                    });
                    checkBoxes[i] = check;

                    selector.addComponent(check);

                    if ((i + 1) % 16 == 0)
                    {
                        selector.addComponent(new JSeparator(SwingConstants.VERTICAL));
                    }
                }
            }
        }

        public void loadParameters(VarMeasureSelector source)
        {
            // load parameter values from source
            if (source != null)
            {
                final Measures[] measures = Measures.getOrderedById();
                final long value = source.getValue().longValue();

                // can't save more than 64 :-(
                for (int i = 0; i < Math.min(measures.length, 64); i++)
                {
                    final boolean selected = (value & (1L << i)) != 0L;

                    measures[i].setSelected(selected);
                }

                // GUI not initialized in headless mode
                if (checkBoxes != null)
                {
                    // update checkbox state
                    ThreadUtil.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            for (int i = 0; i < Math.min(measures.length, 64); i++)
                            {
                                final boolean selected = measures[i].isSelected();

                                if (checkBoxes[i].isSelected() != selected)
                                    checkBoxes[i].setSelected(selected);
                            }
                        }
                    });
                }
            }
        }

        public void saveParameters(VarMeasureSelector source)
        {
            // update parameter values to source
            if (source != null)
            {
                final Measures[] measures = Measures.getOrderedById();
                long value = 0;

                // can't save more than 64 :-(
                for (int i = 0; i < Math.min(checkBoxes.length, 64); i++)
                    if (measures[i].isSelected())
                        value |= 1L << i;

                // update value in trigger
                source.setValue(Long.valueOf(value));
            }
        }

        @Override
        public void triggered(VarMeasureSelector source)
        {
            // should not happen but just in case...
            if (Icy.getMainInterface().isHeadLess())
                return;

            loadParameters(source);

            selector.pack();
            selector.showDialog(true);

            saveParameters(source);

            // save selection change into plugin preferences only in interactive mode (don't
            // do it with Protocols)
            if (!isHeadLess() && measureSelectionChanged)
            {
                // save settings
                final XMLPreferences prefs = getPreferences("measures");

                for (Measures measure : Measures.values())
                    prefs.node(measure.name()).putBoolean("selected", measure.isSelected());

                book.setValue(null);

                execute();
            }
        }

        @Override
        public void valueChanged(Var<Long> source, Long oldValue, Long newValue)
        {
            // set parameters from value (in case we modify value from external
            loadParameters((VarMeasureSelector) source);
        }

        @Override
        public void referenceChanged(Var<Long> source, Var<? extends Long> oldReference,
                Var<? extends Long> newReference)
        {
            //
        }
    }

    public static void main(String[] args)
    {
        Icy.main(args);

        PluginLauncher.start(PluginLoader.getPlugin(ROIMeasures.class.getName()));
    }
}
