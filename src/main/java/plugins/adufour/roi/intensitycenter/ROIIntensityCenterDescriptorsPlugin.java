package plugins.adufour.roi.intensitycenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.type.point.Point3D;
import icy.type.rectangle.Rectangle5D;
import plugins.kernel.roi.roi2d.ROI2DPoint;
import plugins.kernel.roi.roi3d.ROI3DPoint;

/**
 * This descriptor uses the image intensities as a weight factor to find the mass center of the ROI. As a result, the intensity mass center is returned.
 * For this, each ROI is scanned twice. First, the image intensity dynamic is analyzed. And second, the weighted intensity is computed with the intensities
 * normalized with respect to the intensity dynamic analysis results.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class ROIIntensityCenterDescriptorsPlugin extends Plugin implements PluginROIDescriptor
{
    public static final String ID_MASS_CENTER_X = ROIIntensityCenterXDescriptor.ID;
    public static final String ID_MASS_CENTER_Y = ROIIntensityCenterYDescriptor.ID;
    public static final String ID_MASS_CENTER_Z = ROIIntensityCenterZDescriptor.ID;

    public static final ROIIntensityCenterXDescriptor intensityCenterXDescriptor = new ROIIntensityCenterXDescriptor();
    public static final ROIIntensityCenterYDescriptor intensityCenterYDescriptor = new ROIIntensityCenterYDescriptor();
    public static final ROIIntensityCenterZDescriptor intensityCenterZDescriptor = new ROIIntensityCenterZDescriptor();

    /**
     * Compute and returns the intensity center of specified ROI.
     * 
     * @param roi
     *        Target ROI.
     * @param sequence
     *        Target sequence.
     * @return Position of the intensity center.
     * @throws InterruptedException
     *         If the thread is interrupted during the descriptors computation.
     */
    public static Point3D computeIntensityCenter(ROI roi, Sequence sequence) throws InterruptedException
    {
        final Rectangle5D bounds = roi.getBounds5D();

        // special case of empty bounds ? --> return position
        if (bounds.isEmpty())
            return new Point3D.Double(bounds.getX(), bounds.getY(), bounds.getZ());
        // special case of single point ? --> return position
        if ((roi instanceof ROI2DPoint) || (roi instanceof ROI3DPoint))
            return new Point3D.Double(bounds.getX(), bounds.getY(), bounds.getZ());
        // single point ROI (advanced check) ? --> return position
        if (((bounds.getSizeX() * bounds.getSizeY()) <= 1d) && (bounds.isInfiniteZ() || (bounds.getSizeZ() <= 1d)))
            return new Point3D.Double(bounds.getX(), bounds.getY(), bounds.getZ());

        final SequenceDataIterator it = new SequenceDataIterator(sequence, roi, true);
        double x, y, z, min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY, sumW;
        long numPts;
        x = 0d;
        y = 0d;
        z = 0d;
        sumW = 0d;
        numPts = 0;

        // intensity dynamic analysis
        while (!it.done())
        {
            max = Math.max(max, it.get());
            min = Math.min(min, it.get());
            it.next();
        }
        final double valueDynamic = (max - min == 0d) ? 1 : max - min;
        final double extra = (max - min == 0d) ? 1 : 0; // Used when dynamic interval is empty (only one intensity in the roi)

        // weighted center calculation
        it.reset();
        while (!it.done())
        {
            // check for interruption sometime
            if (((numPts & 0xFFFF) == 0) && Thread.currentThread().isInterrupted())
                throw new InterruptedException();

            final double weight = (it.get() + extra - min) / valueDynamic; // normalized
            x += (it.getPositionX()) * weight;
            y += (it.getPositionY()) * weight;
            z += (it.getPositionZ()) * weight;
            sumW += weight;
            numPts++;
            it.next();
        }

        return new Point3D.Double(x / sumW, y / sumW, z / sumW);
    }

    @Override
    public List<ROIDescriptor> getDescriptors()
    {
        final List<ROIDescriptor> result = new ArrayList<ROIDescriptor>();

        result.add(intensityCenterXDescriptor);
        result.add(intensityCenterYDescriptor);
        result.add(intensityCenterZDescriptor);

        return result;
    }

    @Override
    public Map<ROIDescriptor, Object> compute(ROI roi, Sequence sequence)
            throws UnsupportedOperationException, InterruptedException
    {
        final Map<ROIDescriptor, Object> result = new HashMap<ROIDescriptor, Object>();

        try
        {
            // compute mass center descriptors
            final Point3D intensityCenter = computeIntensityCenter(roi, sequence);

            result.put(intensityCenterXDescriptor,
                    Double.valueOf(ROIIntensityCenterXDescriptor.getIntensityCenterX(intensityCenter)));
            result.put(intensityCenterYDescriptor,
                    Double.valueOf(ROIIntensityCenterYDescriptor.getIntensityCenterY(intensityCenter)));
            result.put(intensityCenterZDescriptor,
                    Double.valueOf(ROIIntensityCenterZDescriptor.getIntensityCenterZ(intensityCenter)));
        }
        catch (Exception e)
        {
            final String mess = getClass().getSimpleName() + ": cannot compute descriptors for '" + roi.getName() + "'";
            throw new UnsupportedOperationException(mess, e);
        }

        return result;
    }

}
