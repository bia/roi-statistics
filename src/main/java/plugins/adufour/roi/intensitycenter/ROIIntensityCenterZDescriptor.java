package plugins.adufour.roi.intensitycenter;

import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.type.point.Point3D;

public class ROIIntensityCenterZDescriptor extends ROIDescriptor
{
    public static final String ID = "Intensity center Z";

    public ROIIntensityCenterZDescriptor()
    {
        super(ID, "Intensity center Z", Double.class);
    }

    @Override
    public String getDescription()
    {
        return "Intensity center Z";
    }

    @Override
    public String getUnit(Sequence sequence)
    {
        return "px";
    }

    @Override
    public boolean separateChannel()
    {
        return true;
    }

    @Override
    public boolean needRecompute(SequenceEvent change)
    {
        return (change.getSourceType() == SequenceEventSourceType.SEQUENCE_DATA);
    }

    @Override
    public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
    {
        return Double.valueOf(
                getIntensityCenterZ(ROIIntensityCenterDescriptorsPlugin.computeIntensityCenter(roi, sequence)));
    }

    /**
     * Returns position Z of specified Point3D object
     * 
     * @param point
     *        Point to use as center.
     * @return Position Z of specified point.
     */
    public static double getIntensityCenterZ(Point3D point)
    {
        if (point == null)
            return Double.NaN;

        return point.getZ();
    }
}
