package plugins.adufour.roi.intensitycenter;

import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.type.point.Point3D;

public class ROIIntensityCenterXDescriptor extends ROIDescriptor
{
    public static final String ID = "Intensity center X";

    public ROIIntensityCenterXDescriptor()
    {
        super(ID, "Intensity center X", Double.class);
    }

    @Override
    public String getDescription()
    {
        return "Intensity center X";
    }

    @Override
    public String getUnit(Sequence sequence)
    {
        return "px";
    }

    @Override
    public boolean separateChannel()
    {
        return true;
    }

    @Override
    public boolean needRecompute(SequenceEvent change)
    {
        return (change.getSourceType() == SequenceEventSourceType.SEQUENCE_DATA);
    }

    @Override
    public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
    {
        return Double.valueOf(
                getIntensityCenterX(ROIIntensityCenterDescriptorsPlugin.computeIntensityCenter(roi, sequence)));
    }

    /**
     * Returns position X of specified Point3D object
     * 
     * @param point
     *        Point to use as center.
     * @return Position X of specified point.
     */
    public static double getIntensityCenterX(Point3D point)
    {
        if (point == null)
            return Double.NaN;

        return point.getX();
    }
}
