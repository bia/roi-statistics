package plugins.adufour.roi;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point3d;

import icy.math.UnitUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.type.point.Point3D;
import plugins.adufour.roi.mesh.Vertex3D;
import plugins.adufour.roi.mesh.polygon.ROI3DPolygonalMesh;
import plugins.kernel.roi.roi2d.ROI2DRectShape;

public class ROIFeretDiameterDescriptor extends Plugin implements PluginROIDescriptor, PluginBundled
{
    public static class ROIFeretDiameter extends ROIDescriptor
    {
        protected ROIFeretDiameter()
        {
            super("Max Feret diameter", Double.class);
        }

        @Override
        public String getDescription()
        {
            return "Feret (aka caliper) diameter (maximum distance between any 2 points of this ROI)";
        }

        @Override
        public String getUnit(Sequence sequence)
        {
            if (sequence == null)
                return "px";

            return UnitUtil.MICRO_STRING + "m";
        }

        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException, InterruptedException
        {
            return computeFeretDiameter(roi, sequence);
        }

        /**
         * @param roi
         *        the {@link ROI} we want to compute feret diameter
         * @param sequence
         *        sequence used to retrieve the pixel size information
         * @return The Feret diameter (maximum distance between any 2 points of this ROI
         * @throws InterruptedException
         */
        public static double computeFeretDiameter(ROI roi, Sequence sequence) throws InterruptedException
        {
            double scaleX = 1, scaleY = 1, scaleZ = 1;

            if (sequence != null)
            {
                scaleX = sequence.getPixelSizeX();
                scaleY = sequence.getPixelSizeY();
                scaleZ = sequence.getPixelSizeZ();
            }

            double maxDistance = 0.0;

            if (roi instanceof ROI2D)
            {
                if (roi instanceof ROI2DRectShape)
                {
                    Rectangle2D bounds = ((ROI2D) roi).getBounds2D();
                    maxDistance = Math.max(bounds.getWidth() * scaleX, bounds.getHeight() * scaleY);
                }
                else
                {
                    Point[] pts = ((ROI2D) roi).getBooleanMask(true).getContourPoints();

                    Point2D pi = new Point2D.Double(), pj = new Point2D.Double();

                    for (int i = 0; i < pts.length; i++)
                    {
                        pi.setLocation(pts[i].getX() * scaleX, pts[i].getY() * scaleY);

                        for (int j = i + 1; j < pts.length; j++)
                        {
                            pj.setLocation(pts[j].getX() * scaleX, pts[j].getY() * scaleY);

                            double distance = pi.distanceSq(pj);
                            if (distance > maxDistance)
                                maxDistance = distance;
                        }
                    }

                    // we have calculated the squared distance for optimization purposes
                    maxDistance = Math.sqrt(maxDistance);
                }
            }
            else if (roi instanceof ROI3D)
            {
                Point3d[] points = null;

                if (roi.getNumberOfPoints() > 100)
                {
                    try
                    {
                        // use the convex hull instead to speed things up
                        roi = Convexify.createConvexROI(roi);
                        List<Vertex3D> vertices = ((ROI3DPolygonalMesh) roi).getVertices();
                        int n = vertices.size();

                        points = new Point3d[n];
                        for (int i = 0; i < n; i++)
                        {
                            Point3d p = new Point3d(vertices.get(i).position);
                            p.x *= scaleX;
                            p.y *= scaleY;
                            p.z *= scaleZ;
                            points[i] = p;
                        }
                    }
                    catch (Throwable t)
                    {
                        // impossible to compute the convex hull
                    }
                }

                if (points == null)
                {
                    Point3D.Integer[] pts = ((ROI3D) roi).getBooleanMask(true).getContourPoints();
                    points = new Point3d[pts.length];

                    for (int i = 0; i < pts.length; i++)
                    {
                        points[i] = new Point3d(pts[i].x * scaleX, pts[i].y * scaleY, pts[i].z * scaleZ);
                    }
                }

                for (int i = 0; i < points.length; i++)
                    for (int j = i + 1; j < points.length; j++)
                    {
                        double distance = points[i].distanceSquared(points[j]);
                        if (distance > maxDistance)
                            maxDistance = distance;
                    }

                // we have calculated the squared distance for optimization purposes
                maxDistance = Math.sqrt(maxDistance);
            }
            else
            {
                System.err.println("Cannot compute Max. Feret diameter for ROI of type: " + roi.getClassName());
                maxDistance = 0.0;
            }

            return maxDistance;
        }
    }

    private static final ROIFeretDiameter feretDiameter = new ROIFeretDiameter();

    @Override
    public List<ROIDescriptor> getDescriptors()
    {
        ArrayList<ROIDescriptor> descriptors = new ArrayList<ROIDescriptor>(1);
        descriptors.add(feretDiameter);
        return descriptors;
    }

    @Override
    public Map<ROIDescriptor, Object> compute(ROI roi, Sequence sequence)
            throws UnsupportedOperationException, InterruptedException
    {
        HashMap<ROIDescriptor, Object> map = new HashMap<ROIDescriptor, Object>();
        map.put(feretDiameter, feretDiameter.compute(roi, sequence));
        return map;
    }

    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
}
